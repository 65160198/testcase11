/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.testcase11;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class Testcase11 {

    public Testcase11() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void TestcheckWinRow2_O_output_true() {
        String [][] table = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void TestcheckWinRow1_O_output_true() {
        String [][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void TestcheckWinRow3_O_output_true() {
        String [][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void TestcheckWinRow3_O_output_false() {
        String [][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
}
